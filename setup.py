from setuptools import setup, find_packages

def format(input, start = 0):
	result = ''
	indent = False
	count = 0

	with open(input, 'r') as file:
		for line in file:
			if count > start:
				if line[:1] == '\t' and not indent:
					indent = True
					result += '::\n\n'

				if line[:1].isalnum() and indent:
					indent = False

				result += line.replace('> ', '\t').replace('>', '\t')
			count += 1

	return result

blurb = ('The Bifröst is a web hook broker to connect Midgard services like'
	' GitHub and BitBucket to Asgard. You can interpret that however you would '
	' like.\n')

setup(
	name = 'bifrost',
	version = '0.0.0',
	author = 'Justin Willis',
	author_email = 'sirJustin.Willis@gmail.com',
	packages = find_packages(),
	include_package_data = True,
	zip_safe = False,
	url = 'https://bitbucket.org/bkvaluemeal/bifrost',
	license = 'ISC License',
	description = 'A web hook broker for coordinating information',
	long_description = blurb + format('README.md', 3),
	entry_points = {
		'console_scripts': [
			'bifrost = bifrost.__main__:main'
		]
	},
	classifiers = [
		'Development Status :: 3 - Alpha',
		'Environment :: Web Environment',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: ISC License (ISCL)',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3',
		'Topic :: Software Development',
		'Topic :: Software Development :: Bug Tracking'
	],
	keywords = 'bifrost web hook broker',
	install_requires = []
)
