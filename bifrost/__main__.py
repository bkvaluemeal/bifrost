import argparse
import bifrost

def main():
	'''
	The main method and entry point of the application
	'''

	parser = argparse.ArgumentParser(
		description = 'bifröst v%s' % bifrost.__version__,
		epilog = 'Copyright (c) 2017, Justin Willis',
		prog = 'bifrost',
		formatter_class = argparse.RawTextHelpFormatter
	)
	parser.add_argument(
		'-v', '--version',
		action = 'version',
		version = 'bifröst v%s' % bifrost.__version__
	)
	args = parser.parse_args()

if __name__ == '__main__':
	main()
